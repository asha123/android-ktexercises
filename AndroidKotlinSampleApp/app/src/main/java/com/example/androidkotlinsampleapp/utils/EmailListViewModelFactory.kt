package com.example.androidkotlinsampleapp.utils

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidkotlinsampleapp.model.EmailListViewModel

class EmailListViewModelFactory (
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EmailListViewModel::class.java)) {
            return EmailListViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}