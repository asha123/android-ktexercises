package com.example.androidkotlinsampleapp.ui

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlinsampleapp.R
import com.example.androidkotlinsampleapp.adapter.EmailItemAdapter
import com.example.androidkotlinsampleapp.databinding.ActivityEmailListBinding
import com.example.androidkotlinsampleapp.model.EmailListViewModel
import com.example.androidkotlinsampleapp.utils.EmailListViewModelFactory

class EmailListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityEmailListBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_email_list)

        val viewModel = ViewModelProviders.of(
            this,
            EmailListViewModelFactory(applicationContext as Application)
        ).get(EmailListViewModel::class.java)

        binding.viewModel = viewModel

        val linearLayoutManager = LinearLayoutManager(
            this, RecyclerView.VERTICAL,false)
        binding.recyclerView.layoutManager = linearLayoutManager


        // Observe the model
        viewModel.emailList.observe(this, Observer{ email_table->
            // Data bind the recycler view
            binding.recyclerView.adapter = EmailItemAdapter(email_table)
        })
    }
}
