package com.example.androidkotlinsampleapp.utils

import com.example.androidkotlinsampleapp.db.EmailEntity
import com.google.gson.annotations.SerializedName

data class  PostData(  @SerializedName("emailId") val emailId : String)

data class  ResponseItems(  @SerializedName("items") val items: List<EmailEntity>?)





