package com.example.androidkotlinsampleapp.repositories

import com.example.androidkotlinsampleapp.utils.ApiWorker
import com.example.androidkotlinsampleapp.utils.NetworkConnectionInterceptor
import com.example.androidkotlinsampleapp.utils.PostData
import com.example.androidkotlinsampleapp.utils.ResponseItems
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.*

interface MyApi {

    @Headers("Content-Type: application/json")
    @POST("list")
    suspend fun emailLogin(
        @Body postData: PostData
    ): Response<ResponseItems>


    companion object {
        operator fun invoke( networkConnectionInterceptor: NetworkConnectionInterceptor): MyApi {

            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl("http://surya-interview.appspot.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ApiWorker.gsonConverter)
                .client(ApiWorker.client)
                .build()
                .create(MyApi::class.java)
        }
    }

}