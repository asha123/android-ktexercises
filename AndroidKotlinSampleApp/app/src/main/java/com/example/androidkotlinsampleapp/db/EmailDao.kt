package com.example.androidkotlinsampleapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface EmailDao {
    @Insert
    suspend fun insert( respString: EmailEntity) : Long

    @Query("SELECT * FROM email_table")
    fun getAllInsertedItems() : LiveData<List<EmailEntity>>
}
