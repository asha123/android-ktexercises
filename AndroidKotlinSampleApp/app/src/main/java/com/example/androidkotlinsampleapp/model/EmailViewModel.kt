package com.example.androidkotlinsampleapp.model

import android.app.Application
import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.androidkotlinsampleapp.db.EmailEntity
import com.example.androidkotlinsampleapp.repositories.EmailRepository
import com.example.androidkotlinsampleapp.ui.EmailListener
import com.example.androidkotlinsampleapp.utils.Coroutines
import com.example.androidkotlinsampleapp.utils.InternetConnectionDetector
import com.example.androidkotlinsampleapp.utils.NetworkConnectionInterceptor


class EmailViewModel(
    val application: Application,
    val networkConnectionInterceptor: NetworkConnectionInterceptor
) : ViewModel() {
    var email: String? = null
    var emailListener: EmailListener? = null

    private val _showSignIn = MutableLiveData<Boolean?>()

    val showListEmail: LiveData<Boolean?>
        get() = _showSignIn

    fun showEmailList() {
        _showSignIn.value = true
    }

    fun onSubmitClicked(view: View) {

        //Email Empty
        if (email.isNullOrEmpty()) {
            emailListener?.onFailure("Email is Empty")
            return
        } else {
            //success

            var localdata = EmailRepository(application,networkConnectionInterceptor).getItems().value
            if(localdata!=null){
                showEmailList()
            }else{

                if(InternetConnectionDetector().isConnectingToInternet(application)){
                    Coroutines.main {
                        val respItems = EmailRepository(application, networkConnectionInterceptor).emailPost(email!!)
                        respItems.items?.forEach {
                            EmailRepository(application, networkConnectionInterceptor).saveItems(
                                EmailEntity(it.emailId,it.imageUrl,it.firstName,it.lastName)
                            )
                        }
                        showEmailList()
                    }
                    }
            }
        }
    }
}
