package com.example.androidkotlinsampleapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.androidkotlinsampleapp.databinding.ListItemsBinding
import com.example.androidkotlinsampleapp.db.EmailEntity

class EmailItemAdapter(private val items: List<EmailEntity>): RecyclerView.Adapter<EmailItemAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ListItemsBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])


    inner class ViewHolder(val binding: ListItemsBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: EmailEntity) {
            binding.item = item
        }
    }
}