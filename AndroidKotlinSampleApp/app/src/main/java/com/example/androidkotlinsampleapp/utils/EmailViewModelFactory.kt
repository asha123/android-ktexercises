package com.example.androidkotlinsampleapp.utils

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.androidkotlinsampleapp.model.EmailViewModel

class EmailViewModelFactory(
    private val application: Application,
    private val networkConnectionInterceptor : NetworkConnectionInterceptor

) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(EmailViewModel::class.java)) {
            return EmailViewModel(application,networkConnectionInterceptor) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}