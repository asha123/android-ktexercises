package com.example.androidkotlinsampleapp.db

import androidx.room.Entity
import androidx.room.PrimaryKey

const val ID = 0

@Entity (tableName = "email_table")
data class EmailEntity(
    var emailId: String? = "",
    var imageUrl: String? = "",
    var firstName: String? = "",
    var lastName: String? = ""
){
    @PrimaryKey(autoGenerate = true)
    var id: Int = ID
}