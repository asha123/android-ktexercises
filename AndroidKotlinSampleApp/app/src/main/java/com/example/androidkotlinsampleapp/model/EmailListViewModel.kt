package com.example.androidkotlinsampleapp.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.androidkotlinsampleapp.db.AppDatabase
import com.example.androidkotlinsampleapp.db.EmailEntity

class EmailListViewModel(application : Application) : AndroidViewModel(application) {
    internal val emailList : LiveData<List<EmailEntity>> = AppDatabase.getInstance(application).emailDao.getAllInsertedItems()
}