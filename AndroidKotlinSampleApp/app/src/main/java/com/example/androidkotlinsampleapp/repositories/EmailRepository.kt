package com.example.androidkotlinsampleapp.repositories

import android.app.Application
import com.example.androidkotlinsampleapp.db.AppDatabase
import com.example.androidkotlinsampleapp.db.EmailEntity
import com.example.androidkotlinsampleapp.utils.NetworkConnectionInterceptor
import com.example.androidkotlinsampleapp.utils.PostData
import com.example.androidkotlinsampleapp.utils.ResponseItems
import com.example.androidkotlinsampleapp.utils.SafeApiRequest

class EmailRepository(
    val application: Application,
    val networkConnectionInterceptor: NetworkConnectionInterceptor
) : SafeApiRequest() {
    suspend fun emailPost(email: String): ResponseItems {
        return apiRequest { MyApi(networkConnectionInterceptor).emailLogin(PostData(email)) }
    }

    suspend fun saveItems(respItem: EmailEntity) =
        AppDatabase.getInstance(application).emailDao.insert(respItem)

    fun getItems() = AppDatabase.getInstance(application).emailDao.getAllInsertedItems()
}