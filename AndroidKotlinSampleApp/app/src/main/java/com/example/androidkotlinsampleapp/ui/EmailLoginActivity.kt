package com.example.androidkotlinsampleapp.ui

import android.app.Application
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.androidkotlinsampleapp.R
import com.example.androidkotlinsampleapp.databinding.ActivityMainBinding
import com.example.androidkotlinsampleapp.model.EmailViewModel
import com.example.androidkotlinsampleapp.utils.EmailViewModelFactory
import com.example.androidkotlinsampleapp.utils.NetworkConnectionInterceptor
import com.example.androidkotlinsampleapp.utils.toast


class EmailLoginActivity : AppCompatActivity(), EmailListener {
    override fun onStarted() {
        toast("onStarted")
    }

    override fun onSuccess(response: String) {
        toast(response)
    }

    override fun onFailure(msg: String) {
        toast("onFailure")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        val viewModel =
            ViewModelProviders.of(this, EmailViewModelFactory(applicationContext as Application,
                NetworkConnectionInterceptor(applicationContext)
            ))
                .get(EmailViewModel::class.java)
        binding.viewModel = viewModel

        viewModel.emailListener = this

        viewModel.showListEmail.observe(this, Observer {
            Intent(this, EmailListActivity::class.java).also {
                it.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(it)
            }
        })

    }
}
