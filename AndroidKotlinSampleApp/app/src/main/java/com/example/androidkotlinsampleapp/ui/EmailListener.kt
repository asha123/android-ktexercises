package com.example.androidkotlinsampleapp.ui

interface EmailListener {
    fun onStarted()
    fun onSuccess(loginResponse: String)
    fun onFailure(msg: String)
}